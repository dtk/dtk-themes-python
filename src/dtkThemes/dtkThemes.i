// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

%module(directors="1") dtkthemes

%include <dtkBase/dtkBase.i>

%{

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

%}

%feature("director") dtkThemesEngineCallBack;
%{

#include <dtkThemes/dtkThemesEngine.h>

%}

// /////////////////////////////////////////////////////////////////
// Macro undefinition
// /////////////////////////////////////////////////////////////////

#undef  DTKTHEMES_EXPORT
#define DTKTHEMES_EXPORT

#undef  Q_INVOKABLE
#define Q_INVOKABLE

// /////////////////////////////////////////////////////////////////
// Wrapper input
// /////////////////////////////////////////////////////////////////

%include "dtkThemesEngine.h"

//
// dtkThemes.i ends here
